#ifndef REGULARBALLRENDER_H
#define REGULARBALLRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLExtraFunctions>
#define PI 3.14159265f
class RegularBallRender
{
public:
    RegularBallRender() = default;
    void initsize(float scale,float aHalf,int n,QImage &img);
    void initsize(float scale,float aHalf,int n,QColor &c);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &light,QVector3D &camera);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QVector<GLfloat> verticesVec_,normalsVec_,textureVec_;
    QOpenGLTexture *texture_{nullptr};

    void devideBall(float r,          //球的半径
                         QVector3D &start, //指向圆弧起点的向量
                         QVector3D &end,   //指向圆弧终点的向量
                         int n,            //圆弧分的份数
                         int i,
                         QVector3D &result3D);           //求第i份在圆弧上的坐标（i为0和n时分别代表起点和终点坐标）
    QVector<double> doolittle(double a[3][4], int length, int cols);
    void readData(double a[3][4],int rowNum,int xnum,double AugMatrix[10][20]);
    void prepareChoose(int times,int rowNum,double AugMatrix[10][20]);
    void choose(int times,int rowNum,int xnum,double AugMatrix[10][20]);
    void resolve(int times,int rowNum,int xnum,double AugMatrix[10][20]);
    void findX(int rowNum,int xnum,double AugMatrix[10][20]);

    void devideLine(QVector3D &start, //线段起点坐标
                         QVector3D &end, //线段终点坐标
                         int n, //线段分的份数
                         int i, QVector3D &result); //求第i份在线段上的坐标（i为0和n时分别代表起点和终点坐标）
};

#endif // REGULARBALLRENDER_H
