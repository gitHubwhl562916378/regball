#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    connect(&tm_,SIGNAL(timeout()),this,SLOT(slotTimeout()));
    tm_.start(200);
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    render_.initsize(2.0,1.6,5,QImage("test.jpg"));
//    render_.initsize(2.0,1.6,5,QColor(Qt::white));
    light_.setX(20);
    light_.setY(3);
    light_.setZ(0);
    camera_.setX(0);
    camera_.setY(0);
    camera_.setZ(3);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(0,0,0),QVector3D(0,1,0));

    QMatrix4x4 mMatrix;
//    mMatrix.rotate(angleX_,1,0,0);
    mMatrix.rotate(angleY_,0,1,0);
//    mMatrix.rotate(angleZ_,0,0,1);
    render_.render(f,pMatrix_,vMatrix,mMatrix,light_,camera_);
}

void Widget::slotTimeout()
{
    angleX_ += 5;
    angleY_ += 5;
    angleZ_ += 5;
    update();
}
