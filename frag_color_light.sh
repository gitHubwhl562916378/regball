#version 330
uniform sampler2D sTexture;
in vec4 vColor;
in vec4 vAmbient;
in vec4 vDiffuse;
in vec4 vSpecular;
out vec4 fragColor;

void main(void)
{
    vec4 finalColor =  vColor * (vAmbient + vDiffuse + vSpecular);
    fragColor = finalColor;
}
